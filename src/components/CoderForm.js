import React, {Component} from 'react';
import {Button, Container, Form, Input, TextArea} from "semantic-ui-react";
import 'semantic-ui-css/semantic.min.css';
import {decodeMessage, encodeMessage} from "../store/action";
import {connect} from "react-redux";

class CoderForm extends Component {

  state = {
    messageForEncode: '',
    messageForDecode: '',
    password: ''
  };

  handleSubmit = event => {
    event.preventDefault();
  };

  encodeMessageHandler = () => {
    if (this.state.password.length > 0) {
      const message = {"password": this.state.password, "message": this.state.messageForEncode};
      this.props.onEncodeMessage(message).then(() => {
        this.setState({password: '', messageForEncode: '', messageForDecode: this.props.encodedText});
      });

    } else alert('password not defined');
  };

  decodeMessageHandler = () => {
    if (this.state.password.length > 0) {
      const message = {"password": this.state.password, "message": this.state.messageForDecode};
      this.props.onDecodeMessage(message).then(() => {
        this.setState({password: '', messageForDecode: '', messageForEncode: this.props.decodedText });
      });
    } else alert('password not defined');
  };

  onChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  render() {
    return(
      <Container>
        <Form onSubmit={this.handleSubmit} style={{marginTop: '50px'}} >
          <Form.Field inline>
            <label>decoded message</label>
            <TextArea placeholder='Input message' name="messageForEncode"
                      value={this.state.messageForEncode}
                      onChange={this.onChangeHandler}/>
          </Form.Field>

          <Form.Field inline>
            <label>Password</label>
            <Input placeholder='Input password' name="password"
                   value={this.state.password} onChange={this.onChangeHandler}/>
            <Button type='submit' primary style={{marginLeft: '20px'}}
                    onClick={this.encodeMessageHandler}>Encode</Button>
            <Button type='submit' secondary style={{marginLeft: '20px'}}
                    onClick={this.decodeMessageHandler}>Decode</Button>
          </Form.Field>

          <Form.Field inline>
            <label>encoded message</label>
            <TextArea placeholder='Input message' name="messageForDecode"
                      value={this.state.messageForDecode}
                      onChange={this.onChangeHandler}/>
          </Form.Field>
        </Form>
      </Container>
    )
  }
}
const mapStateToProps = state => {
  return {
    encodedText: state.encodedText,
    decodedText: state.decodedText
  }
};
const mapDispatchToProps = dispatch => {
  return {
    onEncodeMessage: message => dispatch(encodeMessage(message)),
    onDecodeMessage: message => dispatch(decodeMessage(message)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(CoderForm);