import React, {Component, Fragment} from 'react';
import CoderForm from "./components/CoderForm";

class App extends Component {
  render() {
    return (
      <Fragment>
        <CoderForm/>
      </Fragment>
    );
  }
}

export default App;
